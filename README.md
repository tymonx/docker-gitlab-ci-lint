# Docker GitLab CI Lint

Docker image that contains tools for linting GitLab CI YAML configuration files `.gitlab-ci.yml`.
